# Milan Holidays

### Tech stack
* HTML/CSS
* Flex
* Normalize CSS

### Demo
Live demo is hosted [here](https://kit-portfolio.gitlab.io/simple-layouts/milan-holidays/)

### Launch
To run the project follow the next steps:
* No magic here - simple HTML

# Preview
![Project preview](preview.png#center)